/**
 ******************************************************************************
 * @file    IsoBus_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/IsoBus/IsoBus_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Protocols
{
   static I_IsoBus* p_IsoBus_Impl = NULL;

   const char* IsoBus_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle IsoBus functions";
   }

   void stub_setImpl(I_IsoBus* pNewImpl)
   {
      p_IsoBus_Impl = pNewImpl;
   }

   static I_IsoBus* IsoBus_Stub_Get_Impl(void)
   {
      if(p_IsoBus_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to IsoBus functions. Did you forget"
                   << "to call Protocols::stub_setImpl() ?" << std::endl;

         throw IsoBus_StubImplNotSetException();
      }

      return p_IsoBus_Impl;
   }

      ///////////////////////////////
      // I_IsoBus Methods Definition //
      ///////////////////////////////

   I_IsoBus::~I_IsoBus()
   {
      if(p_IsoBus_Impl == this)
      {
         p_IsoBus_Impl = NULL;
      }
   }

   ///////////////////////////////////
   // _Mock_IsoBus Methods Definition //
   ///////////////////////////////////

   _Mock_IsoBus::_Mock_IsoBus()
   {
   }

               //////////////////////////////////
               // Definition of Non Stub Class //
               //////////////////////////////////

   IsoBus::IsoBus()
   {
   }

   IsoBus::~IsoBus()
   {
   }
}
