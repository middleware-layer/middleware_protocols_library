/**
 ******************************************************************************
 * @file    IsoBus_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_ISOBUS_ISOBUS_STUB_HPP
#define SOURCE_TESTING_STUBS_ISOBUS_ISOBUS_STUB_HPP

#include <Production/IsoBus/IsoBus.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Protocols
{
    class IsoBus_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_IsoBus
   {
      public:
         virtual ~I_IsoBus();
   };

   class _Mock_IsoBus : public I_IsoBus
   {
      public:
         _Mock_IsoBus();
   };

   typedef ::testing::NiceMock<_Mock_IsoBus> Mock_IsoBus;

   void stub_setImpl(I_IsoBus* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_ISOBUS_ISOBUS_STUB_HPP
