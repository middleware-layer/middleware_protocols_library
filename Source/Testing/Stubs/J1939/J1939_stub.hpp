/**
 ******************************************************************************
 * @file    J1939_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_J1939_J1939_STUB_HPP
#define SOURCE_TESTING_STUBS_J1939_J1939_STUB_HPP

#include <Production/J1939/J1939.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Protocols
{
    namespace J1939
    {
        class J1939_StubImplNotSetException : public std::exception
       {
          public:
             virtual const char* what(void) const throw ();
       };

       class I_J1939
       {
          public:
             virtual ~I_J1939();
       };

       class _Mock_J1939 : public I_J1939
       {
          public:
             _Mock_J1939();
       };

       typedef ::testing::NiceMock<_Mock_J1939> Mock_J1939;

       void stub_setImpl(I_J1939* pNewImpl);
    }
}

#endif // SOURCE_TESTING_STUBS_J1939_J1939_STUB_HPP
