/**
 ******************************************************************************
 * @file    J1939_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/J1939/J1939_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Protocols
{
    namespace J1939
    {
       static I_J1939* p_J1939_Impl = NULL;

       const char* J1939_StubImplNotSetException::what(void) const throw()
       {
          return "No stub implementation is set to handle J1939 functions";
       }

       void stub_setImpl(I_J1939* pNewImpl)
       {
          p_J1939_Impl = pNewImpl;
       }

       static I_J1939* J1939_Stub_Get_Impl(void)
       {
          if(p_J1939_Impl == NULL)
          {
             std::cerr << "ERROR: No Stub implementation is currently set to handle "
                       << "calls to J1939 functions. Did you forget"
                       << "to call Protocols::J1939::stub_setImpl() ?" << std::endl;

             throw J1939_StubImplNotSetException();
          }

          return p_J1939_Impl;
       }

          ///////////////////////////////
          // I_J1939 Methods Definition //
          ///////////////////////////////

       I_J1939::~I_J1939()
       {
          if(p_J1939_Impl == this)
          {
             p_J1939_Impl = NULL;
          }
       }

       ///////////////////////////////////
       // _Mock_J1939 Methods Definition //
       ///////////////////////////////////

       _Mock_J1939::_Mock_J1939()
       {
       }

                   //////////////////////////////////
                   // Definition of Non Stub Class //
                   //////////////////////////////////

       J1939::J1939()
       {
       }

       J1939::~J1939()
       {
       }
    }
}
