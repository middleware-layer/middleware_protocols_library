/**
 ******************************************************************************
 * @file    J1939Mapping.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    17 de jan de 2018
 * @brief
 ******************************************************************************
 */

#include <Production/J1939/J1939.hpp>

namespace Protocols
{
   namespace J1939
   {
      template<>
      auto populateSPNMap(std::map<suspectParameterNumber, SPN_Definition<float>> &spnMap) -> void
      {
         spnMap[21] = SPN::ENGINE_ECU_TEMPERATURE;
         spnMap[22] = SPN::EXTENDED_CRANKCASE_BLOW_BY_PRESSURE;
         spnMap[51] = SPN::THROTTLE_POSITION;
         spnMap[53] = SPN::TRANSMISSION_SYNCHRONIZER_CLUTCH_VALUE;
         spnMap[54] = SPN::TRANSMISSION_SYNCHRONIZER_BRAKE_VALUE;
         spnMap[59] = SPN::SHIFT_FINGER_GEAR_POSITION;
         spnMap[60] = SPN::SHIFT_FINGER_RAIL_POSITION;
         spnMap[72] = SPN::BLOWER_BYPASS_VALVE_POSITION;
         spnMap[79] = SPN::ROAD_SURFACE_TEMPERATURE;
         spnMap[80] = SPN::WASHER_FLUID_LEVEL;
         spnMap[81] = SPN::PARTICULATE_TRAP_INLET_PRESSURE;
         spnMap[84] = SPN::WHEEL_BASED_VEHICLE_SPEED;
         spnMap[91] = SPN::ACCELERATOR_PEDAL_POSITION_1;
         spnMap[96] = SPN::FUEL_LEVEL;
         spnMap[98] = SPN::ENGINE_OIL_LEVEL;
         spnMap[99] = SPN::ENGINE_OIL_FILTER_DIFFERENTIAL_PRESSURE;
         spnMap[101] = SPN::CRANKCASE_PRESSURE;
         spnMap[107] = SPN::AIR_FILTER_1_DIFFERENTIAL_PRESSURE;
         spnMap[108] = SPN::BAROMETRIC_PRESSURE;
         spnMap[111] = SPN::COOLANT_LEVEL;
         spnMap[112] = SPN::COOLANT_FILTER_DIFFERENTIAL_PRESSURE;
         spnMap[124] = SPN::TRANSMISSION_OIL_LEVEL;
         spnMap[132] = SPN::INLET_AIR_MASS_FLOW_RATE;
         spnMap[136] = SPN::AUXILIARY_VACUUM_PRESSURE_READING;
         spnMap[137] = SPN::AUXILIARY_GAGE_PRESSURE_READING_1;
      }

      template<>
      auto populateSPNMap(std::map<suspectParameterNumber, SPN_Definition<int16_t>> &spnMap) -> void
      {
         spnMap[52] = SPN::ENGINE_INTERCOOLER_TEMPERATURE;
         spnMap[75] = SPN::STEERING_AXLE_TEMPERATURE;
         spnMap[90] = SPN::POWER_TAKEOFF_OIL_TEMPERATURE;
         spnMap[105] = SPN::INTAKE_MANIFOLD_1_TEMPERATURE;
         spnMap[110] = SPN::ENGINE_COOLANT_TEMPERATURE;
         spnMap[120] = SPN::HYDRAULIC_RETARDER_OIL_PRESSURE;
      }

      template<>
      auto populateSPNMap(std::map<suspectParameterNumber, SPN_Definition<uint16_t>> &spnMap) -> void
      {
         spnMap[16] = SPN::FUEL_FILTER_SECTION_SIDE_DIFFERENTIAL_PRESSURE;
         spnMap[73] = SPN::AUXILIARY_PUMP_PRESSURE;
         spnMap[82] = SPN::AIR_START_PRESSURE;
         spnMap[94] = SPN::FUEL_DELIVERY_PRESSURE;
         spnMap[95] = SPN::FUEL_FILTER_DIFFERENTIAL_PRESSURE;
         spnMap[100] = SPN::ENGINE_OIL_PRESSURE;
         spnMap[102] = SPN::BOOST_PRESSURE;
         spnMap[104] = SPN::TURBOCHARGER_LUBE_OIL_PRESSURE_1;
         spnMap[106] = SPN::AIR_INLET_PRESSURE;
         spnMap[109] = SPN::COOLANT_PRESSURE;
         spnMap[116] = SPN::BRAKE_APPLICATION_PRESSURE;
         spnMap[117] = SPN::BRAKE_PRIMARY_PRESSURE;
         spnMap[118] = SPN::BRAKE_SECONDARY_PRESSURE;
         spnMap[119] = SPN::HYDRAULIC_RETARDER_PRESSURE;
         spnMap[123] = SPN::CLUTCH_PRESSURE;
         spnMap[126] = SPN::TRANSMISSION_FILTER_DIFFERENTIAL_PRESSURE;
         spnMap[127] = SPN::TRANSMISSION_OIL_PRESSURE;
      }

      template<>
      auto populateSPNMap(std::map<suspectParameterNumber, SPN_Definition<uint8_t>> &spnMap) -> void
      {
         spnMap[39] = SPN::TIRE_PRESSURE_CHECK_INTERVAL;
         spnMap[46] = SPN::PNEUMATIC_SUPPLY_PRESSURE;
         spnMap[69] = SPN::TWO_SPEED_AXLE_SWITCH;
         spnMap[70] = SPN::PARKING_BRAKE_SWITCH;
         spnMap[74] = SPN::MAXIMUM_VEHICLE_SPEED_LIMIT;
         spnMap[86] = SPN::CRUISE_CONTROL_SET_SPEED;
         spnMap[87] = SPN::CRUISE_CONTROL_HIGH_SET_LIMIT_SPEED;
         spnMap[88] = SPN::CRUISE_CONTROL_LOW_SET_LIMIT_SPEED;
         spnMap[92] = SPN::PERCENT_LOAD_AT_CURRENT_SPEED;
         spnMap[97] = SPN::WATER_IN_FUEL_INDICATOR;
         spnMap[115] = SPN::ALTERNATOR_CURRENT;
      }

      template<>
      auto populateSPNMap(std::map<suspectParameterNumber, SPN_Definition<int8_t>> &spnMap) -> void
      {
         spnMap[114] = SPN::NET_BATTERY_CURRENT;
      }

      template<>
      auto populateSPNMap(std::map<suspectParameterNumber, SPN_Definition<uint32_t>> &spnMap) -> void
      {
         spnMap[103] = SPN::TURBOCHARGER_1_SPEED;
      }

      auto populatePGNMap(std::map<parameterGroupNumber, PGN_Definition> &pgnMap) -> void
      {
         pgnMap[0] = PGN::TORQUE_SPEED_CONTROL_1;
         pgnMap[256] = PGN::TRANSMISSION_CONTROL_1;
         pgnMap[52992] = PGN::CONTINUOUS_TORQUE_AND_SPEED_LIMIT_REQUEST;
         pgnMap[53248] = PGN::CAB_ILLUMINATION_MESSAGE;
         pgnMap[53504] = PGN::AIR_SUSPENSION_CONTROL_6;
         pgnMap[53760] = PGN::AIR_SUSPENSION_CONTROL_2;
         pgnMap[54528] = PGN::TIME_DATE_ADJUST;
         pgnMap[56320] = PGN::ANTI_THEFT_STATUS;
         pgnMap[56576] = PGN::ANTI_THEFT_REQUEST;
         pgnMap[56832] = PGN::RESET;
         pgnMap[57344] = PGN::CAB_MESSAGE_1;
         pgnMap[61440] = PGN::ELECTRONIC_RETARDER_CONTROLLER_1;
         pgnMap[61441] = PGN::ELECTRONIC_BRAKE_CONTROLLER_1;
         pgnMap[61442] = PGN::ELECTRONIC_TRANSMISSION_CONTROLLER_1;
         pgnMap[61443] = PGN::ELECTRONIC_ENGINE_CONTROLLER_2;
         pgnMap[61444] = PGN::ELECTRONIC_ENGINE_CONTROLLER_1;
         pgnMap[61445] = PGN::ELECTRONIC_TRANSMISSION_CONTROLLER_2;
         pgnMap[61446] = PGN::ELECTRONIC_AXLE_CONTROLLER_1;
         pgnMap[61447] = PGN::FORWARD_LANE_IMAGE_URGENT_MSG;
         pgnMap[61448] = PGN::HYDRAULIC_PRESSURE_GOVERNOR_INFO;
         pgnMap[61449] = PGN::VEHICLE_DYNAMIC_STABILITY_CONTROL_2;
         pgnMap[61450] = PGN::ENGINE_GAS_FLOW_RATE;
         pgnMap[64977] = PGN::FMS_STANDARD_INTERFACE_IDENTITY_CAPABILITIES;
         pgnMap[64978] = PGN::ECU_PERFORMANCE;
         pgnMap[64979] = PGN::TURBOCHARGER_INFORMATION_6;
         pgnMap[64980] = PGN::CAB_MESSAGE_3;
         pgnMap[64981] = PGN::ELECTRONIC_ENGINE_CONTROLLER_5;
         pgnMap[64982] = PGN::BASIC_JOYSTICK_MESSAGE_1;
         pgnMap[64983] = PGN::EXTENDED_JOYSTICK_MESSAGE_1;
         pgnMap[64984] = PGN::BASIC_JOYSTICK_MESSAGE_2;
         pgnMap[64985] = PGN::EXTENDED_JOYSTICK_MESSAGE_2;
         pgnMap[64986] = PGN::BASIC_JOYSTICK_MESSAGE_3;
         pgnMap[64987] = PGN::EXTENDED_JOYSTICK_MESSAGE_3;
         pgnMap[64988] = PGN::MARINE_CONTROL_INFORMATION;
         pgnMap[64991] = PGN::FRONT_WHEEL_DRIVE_STATUS;
         pgnMap[64992] = PGN::AMBIENT_CONDITIONS_2;
         pgnMap[64993] = PGN::CAB_A_C_CLIMATE_SYSTEM_INFORMATION;
         pgnMap[64994] = PGN::SUPPLY_PRESSURE_DEMAND;
         pgnMap[64995] = PGN::EQUIPMENT_OPERATION_AND_CONTROL;
         pgnMap[64996] = PGN::EQUIPMENT_PERFORMANCE_DATA;
         pgnMap[64997] = PGN::MAXIMUM_VEHICLE_SPEED_LIMIT_STATUS;
         pgnMap[64998] = PGN::HYDRAULIC_BRAKING_SYSTEM;
         pgnMap[65031] = PGN::EXHAUST_TEMPERATURE;
         pgnMap[65088] = PGN::LIGHTING_DATA;
         pgnMap[65089] = PGN::LIGHTING_COMMAND;
         pgnMap[65099] = PGN::TRANSMISSION_CONFIGURATION_2;
         pgnMap[65100] = PGN::MILITARY_LIGHTING_COMMAND;
         pgnMap[65101] = PGN::TOTAL_AVERAGED_INFORMATION;
         pgnMap[65102] = PGN::DOOR_CONTROL;
         pgnMap[65103] = PGN::VEHICLE_DYNAMIC_STABILITY_CONTROL_1;
         pgnMap[65104] = PGN::BATTERY_TEMPERATURE;
         pgnMap[65105] = PGN::ADAPTIVE_CRUISE_CONTROL_OPERATOR_INPUT;
         pgnMap[65106] = PGN::VEHICLE_ELECTRICAL_POWER_3;
         pgnMap[65107] = PGN::RETARDER_CONTINUOUS_TORQUE_AND_SPEED_LIMIT;
         pgnMap[65108] = PGN::ENGINE_CONTINUOUS_TORQUE_AND_SPEED_LIMIT;
         pgnMap[65109] = PGN::GASEOUS_FUEL_PROPERTIES;
         pgnMap[65110] = PGN::TANK_INFORMATION_1;
         pgnMap[65111] = PGN::AIR_SUSPENSION_CONTROL_5;
         pgnMap[65112] = PGN::AIR_SUSPENSION_CONTROL_4;
         pgnMap[65113] = PGN::AIR_SUSPENSION_CONTROL_3;
         pgnMap[65114] = PGN::AIR_SUSPENSION_CONTROL_1;
         pgnMap[65115] = PGN::FORWARD_LANE_IMAGE;
         pgnMap[65126] = PGN::BATTERY_MAIN_SWITCH_INFORMATION;
         pgnMap[65127] = PGN::CLIMATE_CONTROL_CONFIGURATION;
         pgnMap[65128] = PGN::VEHICLE_FLUIDS;
         pgnMap[65129] = PGN::ENGINE_TEMPERATURE_3;
         pgnMap[65130] = PGN::ENGINE_FUEL_LUBE_SYSTEMS;
         pgnMap[65131] = PGN::DRIVERS_IDENTIFICATION;
         pgnMap[65132] = PGN::TACOGRAPH;
         pgnMap[65133] = PGN::HEATER_INFORMATION;
         pgnMap[65134] = PGN::HIGH_RESOLUTION_WHEEL_SPEED;
         pgnMap[65135] = PGN::ADPTIVE_CRUISE_CONTROL;
         pgnMap[65136] = PGN::COMBINATION_VEHICLE_WEIGHT;
         pgnMap[65137] = PGN::LASER_TRACER_POSITION;
         pgnMap[65138] = PGN::LASER_LEVELING_SYSTEM_BLADE_CONTROL;
         pgnMap[65139] = PGN::LASER_RECEIVER_MAST_POSITION;
         pgnMap[65140] = PGN::MODIFY_LEVELING_SYSTEM_CONTROL_SET_POINT;
         pgnMap[65141] = PGN::LASER_LEVELING_SYSTEM_VERTICAL_DEVIATION;
         pgnMap[65142] = PGN::LASER_LEVELING_SYSTEM_VERTICAL_POSITION_DISPLAY_DATA;
         pgnMap[65143] = PGN::AUXILIARY_PRESSURES;
         pgnMap[65144] = PGN::TIRE_PRESSURE_CONTROL_UNIT_MODE_AND_STATUS;
         pgnMap[65145] = PGN::TIRE_PRESSURE_CONTROL_UNIT_TARGET_PRESSURES;
         pgnMap[65146] = PGN::TIRE_PRESSURE_CONTROL_UNIT_CURRENT_PRESSURES;
         pgnMap[65147] = PGN::COMBUSTION_TIME_1;
         pgnMap[65148] = PGN::COMBUSTION_TIME_2;
         pgnMap[65149] = PGN::COMBUSTION_TIME_3;
         pgnMap[65150] = PGN::COMBUSTION_TIME_4;
         pgnMap[65151] = PGN::COMBUSTION_TIME_5;
         pgnMap[65152] = PGN::COMBUSTION_TIME_6;
         pgnMap[65153] = PGN::FUEL_INFORMATION_2_GASEOUS;
         pgnMap[65154] = PGN::IGNITION_TIMING_1;
         pgnMap[65155] = PGN::IGNITION_TIMING_2;
         pgnMap[65156] = PGN::IGNITION_TIMING_3;
         pgnMap[65157] = PGN::IGNITION_TIMING_4;
         pgnMap[65158] = PGN::IGNITION_TIMING_5;
         pgnMap[65159] = PGN::IGNITION_TIMING_6;
         pgnMap[65160] = PGN::IGNITION_TRANSFORMER_SECONDARY_OUTPUT_1;
         pgnMap[65161] = PGN::IGNITION_TRANSFORMER_SECONDARY_OUTPUT_2;
         pgnMap[65162] = PGN::IGNITION_TRANSFORMER_SECONDARY_OUTPUT_3;
         pgnMap[65163] = PGN::GASEOUS_FUEL_PRESSURE;
         pgnMap[65164] = PGN::AUXILIARY_ANALOG_INFORMATION;
         pgnMap[65165] = PGN::VEHICLE_ELECTRICAL_POWER_2;
         pgnMap[65166] = PGN::SERVICE_2;
         pgnMap[65167] = PGN::SUPPLY_PRESSURE_2;
         pgnMap[65168] = PGN::ENGINE_TORQUE_HISTORY;
         pgnMap[65169] = PGN::FUEL_LEAKAGE;
         pgnMap[65170] = PGN::ENGINE_INFORMATION;
         pgnMap[65171] = PGN::ENGINE_ELECTRICAL_SYSTEM_MODULE_INFORMATION;
         pgnMap[65172] = PGN::ENGINE_AUXILIARY_COOLANT;
         pgnMap[65173] = PGN::REBUILD_INFORMATION;
         pgnMap[65174] = PGN::TURBOCHARGER_WATEGATE;
         pgnMap[65175] = PGN::TURBOCHARGER_INFORMATION_5;
         pgnMap[65176] = PGN::TURBOCHARGER_INFORMATION_4;
         pgnMap[65177] = PGN::TURBOCHARGER_INFORMATION_3;
         pgnMap[65178] = PGN::TURBOCHARGER_INFORMATION_2;
         pgnMap[65179] = PGN::TURBOCHARGER_INFORMATION_1;
         pgnMap[65180] = PGN::MAIN_BEARING_TEMPERATURE_3;
         pgnMap[65181] = PGN::MAIN_BEARING_TEMPERATURE_2;
         pgnMap[65182] = PGN::MAIN_BEARING_TEMPERATURE_1;
         pgnMap[65183] = PGN::EXHAUST_PORT_TEMPERATURE_5;
         pgnMap[65184] = PGN::EXHAUST_PORT_TEMPERATURE_4;
         pgnMap[65185] = PGN::EXHAUST_PORT_TEMPERATURE_3;
         pgnMap[65186] = PGN::EXHAUST_PORT_TEMPERATURE_2;
         pgnMap[65187] = PGN::EXHAUST_PORT_TEMPERATURE_1;
         pgnMap[65188] = PGN::ENGINE_TEMPERATURE_2;
         pgnMap[65189] = PGN::INTAKE_MANIFOLD_INFORMATION_2;
         pgnMap[65190] = PGN::INTAKE_MANIFOLD_INFORMATION_1;
         pgnMap[65191] = PGN::ALTERNATOR_TEMPERATURE;
         pgnMap[65192] = PGN::ARTICULATION_CONTROL;
         pgnMap[65193] = PGN::EXHAUST_OXYGEN_1;
         pgnMap[65194] = PGN::ALTERNATE_FUEL_2;
         pgnMap[65195] = PGN::ELECTRONIC_TRANSMISSION_CONTROLLER_6;
         pgnMap[65196] = PGN::WHEEL_BRAKE_LINING_REMAINING_INFORMATION;
         pgnMap[65197] = PGN::WHEEL_APPLICATION_PRESSURE_HIGH_RANGE_INFORMATION;
         pgnMap[65198] = PGN::AIR_SUPPLY_PRESSURE;
         pgnMap[65199] = PGN::FUEL_CONSUMPTION_GASEOUS;
         pgnMap[65200] = PGN::TRIP_TIME_INFORMATION_2;
         pgnMap[65201] = PGN::ECU_HISTORY;
         pgnMap[65202] = PGN::FUEL_INFORMATION_1_GASEOUS;
         pgnMap[65203] = PGN::FUEL_INFORMATION_LIQUID;
         pgnMap[65204] = PGN::TRIP_TIME_INFORMATION_1;
         pgnMap[65205] = PGN::TRIP_SHUTDOWN_INFORMATION;
         pgnMap[65206] = PGN::TRIP_VEHICLE_SPEED_CRUISE_DISTANCE_INFORMATION;
         pgnMap[65207] = PGN::ENGINE_SPEED_LOAD_FACTOR_INFORMATION;
         pgnMap[65208] = PGN::TRIP_FUEL_INFORMATION_GASEOUS;
         pgnMap[65209] = PGN::TRIP_FUEL_INFORMATION_LIQUID;
         pgnMap[65210] = PGN::TRIP_DISTANCE_INFORMATION;
         pgnMap[65211] = PGN::TRIP_FAN_INFORMATION;
         pgnMap[65212] = PGN::COMPRESSION_SERVICE_BRAKE_INFORMATION;
         pgnMap[65213] = PGN::FAN_DRIVE;
         pgnMap[65214] = PGN::ELECTRONIC_ENGINE_CONTROLLER_4;
         pgnMap[65215] = PGN::WHEEL_SPEED_INFORMATION;
         pgnMap[65216] = PGN::SERVICE_INFORMATION;
         pgnMap[65217] = PGN::HIGH_RESOLUTION_VEHICLE_DISTANCE;
         pgnMap[65218] = PGN::ELECTRONIC_RETARDER_CONTROLLER_2;
         pgnMap[65219] = PGN::ELECTRONIC_TRANSMISSION_CONTROLLER_5;
         pgnMap[65221] = PGN::ELECTRONIC_TRANSMISSION_CONTROLLER_4;
         pgnMap[65223] = PGN::ELECTRONIC_TRANSMISSION_CONTROLLER_3;
         pgnMap[65237] = PGN::ALTERNATOR_SPEED;
         pgnMap[65241] = PGN::AUXILIARY_INPUT_OUTPUT_STATUS;
         pgnMap[65242] = PGN::SOFTWARE_IDENTIFICATION;
         pgnMap[65243] = PGN::ENGINE_FLUID_LEVEL_PRESSURE_2;
         pgnMap[65244] = PGN::IDLE_OPERATION;
         pgnMap[65245] = PGN::TURBOCHARGER;
         pgnMap[65246] = PGN::AIR_START_PRESSURE;
         pgnMap[65247] = PGN::ELECTRONIC_ENGINE_CONTROLLER_3;
         pgnMap[65248] = PGN::VEHICLE_DISTANCE;
         pgnMap[65249] = PGN::RETARDER_CONFIGURATION;
         pgnMap[65250] = PGN::TRANSMISSION_CONFIGURATION;
         pgnMap[65251] = PGN::ENGINE_CONFIGURATION;
         pgnMap[65252] = PGN::SHUTDOWN;
         pgnMap[65253] = PGN::ENGINE_HOURS_REVOLUTIONS;
         pgnMap[65254] = PGN::TIME_DATE;
         pgnMap[65255] = PGN::VEHICLE_HOURS;
         pgnMap[65256] = PGN::VEHICLE_DIRECTION_SPEED;
         pgnMap[65257] = PGN::FUEL_CONSUMPTION_LIQUID;
         pgnMap[65258] = PGN::VEHICLE_WEIGHT;
         pgnMap[65259] = PGN::COMPONENT_IDENTIFICATION;
         pgnMap[65260] = PGN::VEHICLE_IDENTIFICATION;
         pgnMap[65261] = PGN::CRUISE_CONTROL_VEHICLE_SPEED_SETUP;
         pgnMap[65262] = PGN::ENGINE_TEMPERATURE_1;
         pgnMap[65263] = PGN::ENGINE_FLUID_LEVEL_PRESSURE_1;
         pgnMap[65264] = PGN::POWER_TAKEOFF_INFORMATION;
         pgnMap[65265] = PGN::CRUISE_CONTROL_VEHICLE_SPEED;
         pgnMap[65266] = PGN::FUEL_ECONOMY_LIQUID;
         pgnMap[65267] = PGN::VEHICLE_POSITION;
         pgnMap[65268] = PGN::TIRE_CONDITION;
         pgnMap[65269] = PGN::AMBIENT_CONDITION;
         pgnMap[65270] = PGN::INLET_EXHAUST_CONDITIONS_1;
         pgnMap[65271] = PGN::VEHICLE_ELECTRICAL_POWER;
         pgnMap[65272] = PGN::TRANSMISSION_FLUIDS;
         pgnMap[65273] = PGN::AXLE_INFORMATION;
         pgnMap[65274] = PGN::BRAKES;
         pgnMap[65275] = PGN::RETARDER_FLUIDS;
         pgnMap[65276] = PGN::DASH_DISPLAY;
         pgnMap[65277] = PGN::ALTERNATE_FUEL_1;
         pgnMap[65278] = PGN::AUXILIARY_WATER_PUMP_PRESSURE;
         pgnMap[65279] = PGN::WATER_IN_FUEL_INDICATOR;
      }
   }
}
