/**
 ******************************************************************************
 * @file    EtherCAT.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef PRODUCTION_ETHERCAT_ETHERCAT_HPP
#define PRODUCTION_ETHERCAT_ETHERCAT_HPP

namespace Protocols
{
   class EtherCAT
   {
      private:

      protected:

      public:
         EtherCAT();

         virtual ~EtherCAT();
   };
}

#endif // PRODUCTION_ETHERCAT_ETHERCAT_HPP
