/**
 ******************************************************************************
 * @file    J1939Standard.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief   The files j1939-71.pdf and j1939-73.pdf were bought from SAE and
 *          are available at Engenharia/Documentacao/Normas/
 ******************************************************************************
 */

#pragma once

#include <Production/J1939/J1939_Types.hpp>

namespace Protocols
{
   namespace J1939
   {
      namespace SPN
      {

      }

      namespace PGN
      {

      }

   }
}
