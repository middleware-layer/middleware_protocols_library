/**
 ******************************************************************************
 * @file    J1939.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#include <Production/J1939/J1939.hpp>

namespace Protocols
{
   namespace J1939
   {
      /**
       * @brief  Null, because instance will be initialized on demand.
       */
      J1939* J1939::instance = 0;

      J1939::J1939()
      {
         ////////////////////
         /// PGN POPULATE ///
         ////////////////////

         populatePGNMap(this->pgnMap);

         ////////////////////
         /// SPN POPULATE ///
         ////////////////////

         populateSPNMap(this->spnMap_float);
         populateSPNMap(this->spnMap_i8);
         populateSPNMap(this->spnMap_i16);
         populateSPNMap(this->spnMap_u8);
         populateSPNMap(this->spnMap_u16);
         populateSPNMap(this->spnMap_u32);

         return;
      }

      J1939::~J1939()
      {
         // TODO Auto-generated destructor stub
      }

      J1939* J1939::getInstance()
      {
         if (instance == 0)
         {
            instance = new J1939();
         }

         return instance;
      }

      template<typename TYPE>
      auto J1939::decodeSPN(SPN_Definition<TYPE> &spnDefinition) -> void
      {
         if (spnDefinition.variablePointer != nullptr)
         {
            if (spnDefinition.resolution == 0)
            {
               ///////////////////////////////////////////////////////////////////////////////////////////
               /// If the resolution is null, it means the SPN in question IS NOT a measuring variable ///
               ///     If this is the case, we just need to copy the value to the pointer variable     ///
               ///////////////////////////////////////////////////////////////////////////////////////////
            }

            else
            {
               ////////////////////////////////////////////////////////////////////////
               /// Since there is a resolution, it means the SPN is a measuring one ///
               /// In this case, it is needed to calculate the real variable value  ///
               ///       Final_Value = Offset + (message_Value * resolution)        ///
               ////////////////////////////////////////////////////////////////////////

               TYPE teste = spnDefinition.dataRangeMin + (100 * spnDefinition.resolution);

               *(spnDefinition.variablePointer) = teste;
            }
         }

         return;
      }

      auto J1939::decodePGN(PGN_Definition &pgnDefinition) -> void
      {
         std::map<suspectParameterNumber, SPN_Definition<float>>::iterator itSPN_float;

         /////////////////////////////////////
         /// First, we check for emptiness ///
         /////////////////////////////////////

         if (pgnDefinition.spnVector.size() == 0)
         {
            return;
         }

         //////////////////////////////////////////////////////////////////////
         /// Then, we iterate through the list of SPN inside the PGN struct ///
         //////////////////////////////////////////////////////////////////////

         for (auto const &it : pgnDefinition.spnVector)
         {
            /////////////////////////////////////////////////
            /// First look at the spnMap with float SPN's ///
            /////////////////////////////////////////////////

            itSPN_float = this->spnMap_float.find(it);

            if (itSPN_float != this->spnMap_float.end())
            {
               this->decodeSPN(itSPN_float->second);
            }
         }

         return;
      }

      auto J1939::decodeMessage(parameterGroupNumber pgn) -> void
      {
         std::map<parameterGroupNumber, PGN_Definition>::iterator itPGN;

         itPGN = this->pgnMap.find(pgn);

         if (itPGN != this->pgnMap.end())
         {
            this->decodePGN(itPGN->second);
         }

         return;
      }

      template<>
      auto J1939::mapVariablesToSPN(std::map<suspectParameterNumber, float*> &variableMap) -> void
      {
         std::map<suspectParameterNumber, SPN_Definition<float>>::iterator itSPN;

         /////////////////////////////////////
         /// First, we check for emptiness ///
         /////////////////////////////////////

         if (variableMap.size() == 0)
         {
            return;
         }

         ///////////////////////////////////////////////////////////////
         /// Then, we iterate through the list of SPN inside the Map ///
         ///////////////////////////////////////////////////////////////

         for (auto const &it : variableMap)
         {
            itSPN = this->spnMap_float.find(it.first);

            if (itSPN != this->spnMap_float.end())
            {
               (itSPN->second).variablePointer = it.second;
            }
         }

         return;
      }

      template<>
      auto J1939::mapVariablesToSPN(std::map<suspectParameterNumber, int8_t*> &variableMap) -> void
      {
         std::map<suspectParameterNumber, SPN_Definition<int8_t>>::iterator itSPN;

         /////////////////////////////////////
         /// First, we check for emptiness ///
         /////////////////////////////////////

         if (variableMap.size() == 0)
         {
            return;
         }

         ///////////////////////////////////////////////////////////////
         /// Then, we iterate through the list of SPN inside the Map ///
         ///////////////////////////////////////////////////////////////

         for (auto const &it : variableMap)
         {
            itSPN = this->spnMap_i8.find(it.first);

            if (itSPN != this->spnMap_i8.end())
            {
               (itSPN->second).variablePointer = it.second;
            }
         }

         return;
      }

      template<>
      auto J1939::mapVariablesToSPN(std::map<suspectParameterNumber, int16_t*> &variableMap) -> void
      {

      }

      template<>
      auto J1939::mapVariablesToSPN(std::map<suspectParameterNumber, uint8_t*> &variableMap) -> void
      {

      }

      template<>
      auto J1939::mapVariablesToSPN(std::map<suspectParameterNumber, uint16_t*> &variableMap) -> void
      {

      }

      template<>
      auto J1939::mapVariablesToSPN(std::map<suspectParameterNumber, uint32_t*> &variableMap) -> void
      {

      }
   }
}
