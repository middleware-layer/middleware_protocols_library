/**
 ******************************************************************************
 * @file    J1939Types.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief   This file defines the structs and enumerators needed for the J1939
 *          protocol.
 *          Everything inside this file is declared inside the Protocol::J1939
 *          namespace
 ******************************************************************************
 */

#pragma once

#include <stdint.h>

#include <map>
#include <initializer_list>
#include <memory>

namespace Protocols
{
   namespace J1939
   {
      typedef uint16_t suspectParameterNumber;
      typedef uint16_t parameterGroupNumber;

      /**
       * @brief   Every SPN can be either Measured or Status
       */
      enum class SPN_TYPE
      {
         MEASURED,
         STATUS
      };

      /**
       * @brief
       */
      enum PGN_PDU_SPECIFIC
      {
         DA = -1
      };

      /**
       * @brief   Every SPN has a transmission repetition rate defined is seconds or milliseconds)
       *          this enumerator only makes it easier to see the real value
       */
      enum TRANSMISSION_REPETITION_RATE
      {
         AS_NEEDED = 0,
         MILLISECOND = 1,
         SECOND = 1000
      };

      /**
       * @brief   This enumerator defines the possible data types
       *          With these values, it is possible to analyse what we are really measuring
       */
      enum class DATA_TYPE
      {
         NONE,
         DEGREE_CELSIUS,
         KILO_PASCAL,
         MINUTE,
         PERCENTUAL,
         KM_PER_HOUR,
         RPM,
         AMPERE,
         KG_PER_HOUR
      };

      /**
       * @brief   This enumerator defines every single possibility regarding data length.
       *          It is measured in bits.
       */
      enum class DATA_LENGTH
      {
         ONE_BIT = 1,
         TWO_BITS = 2,
         THREE_BITS = 3,
         FOUR_BITS = 4,
         FIVE_BITS = 5,
         SIX_BITS = 6,
         SEVEN_BITS = 7,
         ONE_BYTE = 8,
         TWO_BYTES = 16,
         THREE_BYTES = 24,
         FOUR_BYTES = 32,
         FIVE_BYTES = 40,
         SIX_BYTES = 48,
         SEVEN_BYTES = 56,
         EIGHT_BYTES = 64,
      };

      /**
       * @brief   Struct representing the J1939 SPN definition.
       */
      template<typename TYPE>
      struct SPN_Definition
      {
            /**
             * @brief
             */
            TYPE* variablePointer;

            /**
             * @brief
             */
            DATA_LENGTH dataLenght;

            /**
             * @brief
             */
            DATA_TYPE dataType;

            /**
             * @brief
             */
            TYPE resolution;

            /**
             * @brief
             */
            TYPE dataRangeMin;

            /**
             * @brief
             */
            TYPE dataRangeMax;

            /**
             * @brief
             */
            SPN_TYPE type;
      };

      /**
       * @brief   Struct representing the J1939 PGN definition.
       */
      typedef struct
      {
            /**
             * @brief when 0 -> As needed
             */
            uint16_t transmissionRepetitionRate_Ms;

            /**
             * @brief
             */
            uint8_t dataPage;

            /**
             * @brief
             */
            uint8_t pduFormat;

            /**
             * @brief
             */
            int16_t pduSpecific;

            /**
             * @brief
             */
            uint8_t defaultPriority;

            /**
             * @brief
             */
            std::initializer_list<suspectParameterNumber> spnVector;
      } PGN_Definition;

      /**
       * @brief   This method will populate the spnMap (passed by reference) with every defined 'TYPE' SPN.
       * @param   spnMap : A reference to the desiredS SPN Map
       */
      template<typename TYPE>
      extern auto populateSPNMap(std::map<suspectParameterNumber, SPN_Definition<TYPE>> &spnMap) -> void;

      /**
       * @brief   This method will populate the pgnMap (passed by reference) with every defined PGN.
       * @param   pgnMap : A reference to the desiredS PGN Map
       */
      extern auto populatePGNMap(std::map<parameterGroupNumber, PGN_Definition> &pgnMap) -> void;
   }
}
