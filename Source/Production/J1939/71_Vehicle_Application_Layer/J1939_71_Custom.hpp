/**
 ******************************************************************************
 * @file    J1939Custom.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#pragma once

#include <Production/J1939/J1939_Types.hpp>

namespace Protocols
{
   namespace J1939
   {
      namespace SPN
      {

      }

      namespace PGN
      {

      }
   }
}
