/**
 ******************************************************************************
 * @file    J1939.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief   The files j1939-71.pdf and j1939-73.pdf were bought from SAE and
 *          are available at Engenharia/Documentacao/Normas/
 ******************************************************************************
 */

#pragma once

#include <Production/J1939/71_Vehicle_Application_Layer/J1939_71_Standard.hpp>
#include <Production/J1939/71_Vehicle_Application_Layer/J1939_71_Custom.hpp>

namespace Protocols
{
   namespace J1939
   {
      /**
       * @brief
       */
      class J1939
      {
         private:
            /**
             * @brief Here will be the instance stored.
             */
            static J1939* instance;

            /**
             * @brief   A single map containing every float SPN on our entire library.
             *          the first value is the SPN, while the second is the SPN definition struct
             */
            std::map<suspectParameterNumber, SPN_Definition<float>> spnMap_float;

            /**
             * @brief   A single map containing every int8_t SPN on our entire library.
             *          the first value is the SPN, while the second is the SPN definition struct
             */
            std::map<suspectParameterNumber, SPN_Definition<int8_t>> spnMap_i8;

            /**
             * @brief   A single map containing every int16_t SPN on our entire library.
             *          the first value is the SPN, while the second is the SPN definition struct
             */
            std::map<suspectParameterNumber, SPN_Definition<int16_t>> spnMap_i16;

            /**
             * @brief   A single map containing every uint8_t SPN on our entire library.
             *          the first value is the SPN, while the second is the SPN definition struct
             */
            std::map<suspectParameterNumber, SPN_Definition<uint8_t>> spnMap_u8;

            /**
             * @brief   A single map containing every uint16_t SPN on our entire library.
             *          the first value is the SPN, while the second is the SPN definition struct
             */
            std::map<suspectParameterNumber, SPN_Definition<uint16_t>> spnMap_u16;

            /**
             * @brief   A single map containing every uint32_t SPN on our entire library.
             *          the first value is the SPN, while the second is the SPN definition struct
             */
            std::map<suspectParameterNumber, SPN_Definition<uint32_t>> spnMap_u32;

            /**
             * @brief   A single map containing every PGN on our entire library.
             *          the first value is the PGN, while the second is the PGN definition struct
             */
            std::map<parameterGroupNumber, PGN_Definition> pgnMap;

            /**
             * @brief   Class constructor method.
             *          It basically populates its SPN and PGN maps with the defined standard and
             *          custom SPN / PGN (defined inside J1939Standard.hpp and J1939Custom.hpp)
             */
            J1939();

            /**
             * @brief   This method decodes a SPN definition struct.
             *          It uses the offset (dataRangeMin), the dataRangeMax and the resolution to
             *          determine the real value of the parameter.
             * @param   spnDefinition : A reference to the SPN struct in question
             */
            template<typename TYPE>
            auto decodeSPN(SPN_Definition<TYPE> &spnDefinition) -> void;

            /**
             * @brief   This method decodes a PGN definition struct.
             *          It iterates through the PGN struct in question and calls the 'decodeSPN'
             *          method for every spn available.
             * @param   pgnDefinition : A reference to the PGN struct in question
             */
            auto decodePGN(PGN_Definition &pgnDefinition) -> void;

         protected:

         public:
            /**
             * @brief   Static access method
             */
            static J1939* getInstance();

            /**
             * @brief   Class destructor method.
             */
            virtual ~J1939();

            /**
             * @brief   This method iterates through the whole PGN Map trying to find a corresponding PGN.
             *          If it success, it calls the 'decodePGN' method.
             * @param   pgn : Integer value representing the PGN to be decoded (received through CAN message)
             */
            auto decodeMessage(parameterGroupNumber pgn) -> void;

            /**
             * @brief   This method will map every variable inside the 'TYPE' parameter map (passed by reference) to
             *          the corresponding SPN Map.
             *          If this function is not called by the Application Layer, it will not be able to decode
             *          a SPN, because the constructor method of this class defines every SPN variable pointer to
             *          'nullptr'.
             * @param   variableMap : Reference to the map to be analysed
             */
            template<typename TYPE>
            auto mapVariablesToSPN(std::map<suspectParameterNumber, TYPE*> &variableMap) -> void;
      };
   }
}
