/**
 ******************************************************************************
 * @file    IsoBus.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef PRODUCTION_ISOBUS_ISOBUS_HPP
#define PRODUCTION_ISOBUS_ISOBUS_HPP

namespace Protocols
{
   class IsoBus
   {
      private:

      protected:

      public:
           IsoBus();

           virtual ~IsoBus();
   };
}

#endif // PRODUCTION_ISOBUS_ISOBUS_HPP
